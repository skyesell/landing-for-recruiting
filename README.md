# Cайт для подбора IT-персонала


### Рекрутинговое агенство «Кейт Спарк» специализируется в подборе ИТ-специалистов для российских компаний в сферах финансов, страхования, ритейла и логистики.

При помощи данного сайта  ИТ-специалисты могут отправить свое резюме, а также оставить заявку на обратный звонок от компании. 

### В блоке "Кого ищете?" можно увидеть направления специалистов, а так же стек, с которыми они работают.
![demo](src/assets/gif/chrome_2x81PJdzub.gif)

### В блоке "Кто уже пользуется услугами" представлены компании, сотрудники которые были найдены при помощи дочерней компании «Кейт Спарк».
![demo](src/assets/gif/chrome_kz2oYl2FoM.gif)

### В конце страницы представлены контакты для связи, также была применена интеграция с картами 2ГИС для отображения актуальной карты города с отображением адреса компании.
![demo](src/assets/gif/chrome_lwsl1pCut2.gif)

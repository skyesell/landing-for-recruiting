import { h } from 'preact'
import style from './style.scss'

export const Background = (props) => (
  <div class={style.background} data-color={props.color}>
    {props.children}
  </div>
)

import { h } from 'preact'
import { useEffect, useLayoutEffect } from 'preact/hooks'
import { returnBodyScrollPosition, setBodyScrollPosition } from '../../lib/scroll'
import closeModalIcon from './close-modal.svg'
import style from './style.scss'

export const Modal = (props) => {
  useEffect(() => {
    const listener = (e) => {
      if (e.key === 'Escape') {
        props.onClose()
      }
    }

    window.addEventListener('keydown', listener)

    return () => window.removeEventListener('keydown', listener)
  }, [props])

  useLayoutEffect(() => {
    setBodyScrollPosition()

    return () => returnBodyScrollPosition()
  }, [])

  const doNotDisturbParents = (e) => e.stopPropagation()

  return (
    <div class={style.overlay} onClick={props.onClose}>
      <div class={style.modal} onClick={doNotDisturbParents}>
        <img class={style.closeIcon} src={closeModalIcon} alt='' onClick={props.onClose} />
        {props.children}
      </div>
    </div>
  )
}

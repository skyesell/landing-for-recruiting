import { h } from 'preact'
import rectImage from './rect.svg'
import style from './style.scss'

const Navigation = () => {
  const onLinkClick = (elementId) => () => {
    const element = document.getElementById(elementId)

    if (element !== null) {
      window.scrollTo({ top: element.getBoundingClientRect().top + window.scrollY - 15, behavior: 'smooth' })
    }
  }

  return (
    <div class={style.nav}>
      <nav>
        <a onClick={onLinkClick('about-us')}>О нас</a>
        <img src={rectImage} alt='' />
        <a onClick={onLinkClick('employees')}>Специалисты</a>
        <img src={rectImage} alt='' />
        <a onClick={onLinkClick('services')}>Услуги</a>
        <img src={rectImage} alt='' />
        <a onClick={onLinkClick('prices')}>Тарифы</a>
        <img src={rectImage} alt='' />
        <a onClick={onLinkClick('companies')}>Партнёры</a>
        <img src={rectImage} alt='' />
        <a onClick={onLinkClick('contacts')}>Контакты</a>
      </nav>
    </div>
  )
}

export default Navigation

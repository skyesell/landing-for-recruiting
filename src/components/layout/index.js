import { h } from 'preact'
import style from './style.scss'

const Layout = (props) => {
  return <div class={style.layout}>{props.children}</div>
}

export default Layout

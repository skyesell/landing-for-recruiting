import { h } from 'preact'
import { Router } from 'preact-router'
import Home from '../routes/home'
import { ModalProvider } from '../lib/modal'

const App = () => (
  <div id='app'>
    <ModalProvider>
      <Router>
        <Home path='/' />
      </Router>
    </ModalProvider>
  </div>
)

export default App

import { h } from 'preact'
import { useEffect, useRef, useState } from 'preact/hooks'
import checkedIcon from './checked.svg'
import uncheckedIcon from './unchecked.svg'
import uncheckedErrorIcon from './unchecked-error.svg'
import checkedErrorIcon from './checked-error.svg'
import disabledCheckedIcon from './checked-disabled.svg'
import disabledUncheckedIcon from './unchecked-disabled.svg'
import style from './style.scss'

export const Checkbox = (props) => {
  const nameRef = useRef(props.name ?? '')
  const [checked, setChecked] = useState(props.value ?? false)

  useEffect(() => {
    if (typeof props.onChange === 'function') {
      props.onChange({ name: nameRef.current, value: checked })
    }
  }, [checked, props.name, props.onChange])

  const onClick = () => {
    if (props.disabled) {
      return
    }

    setChecked((state) => !state)
  }

  const hasErrors = props.errors?.length > 0

  const icon = (() => {
    if (props.disabled) {
      return checked ? disabledCheckedIcon : disabledUncheckedIcon
    }

    if (hasErrors) {
      return checked ? checkedErrorIcon : uncheckedErrorIcon
    }

    return checked ? checkedIcon : uncheckedIcon
  })()

  return (
    <div class={style.checkbox} data-disabled={props.disabled} onClick={onClick}>
      <img src={icon} alt='' />
    </div>
  )
}

import { h } from 'preact'
import { useLayoutEffect, useState } from 'preact/hooks'
import logoImage from '../../assets/logo.svg'
import menuOpenedImage from '../../assets/menu-opened.svg'
import menuClosedImage from '../../assets/menu-closed.svg'
import { useModal } from '../../lib/modal'
import { returnBodyScrollPosition, setBodyScrollPosition } from '../../lib/scroll'
import style from './style.scss'

const Header = () => {
  const [isOpened, setIsOpened] = useState(false)
  const [elementId, setElementId] = useState('')
  const modalController = useModal()

  const menuIcon = isOpened ? menuOpenedImage : menuClosedImage

  const toggleMenu = () => setIsOpened((state) => !state)

  const onMobileLinkClick = (blockId) => (e) => {
    e.preventDefault()

    setIsOpened(false)
    setElementId(blockId)
  }

  const contactMeClicked = (e) => {
    e.preventDefault()
    modalController.setContactManagerModalState(true)
  }

  useLayoutEffect(() => {
    isOpened ? setBodyScrollPosition() : returnBodyScrollPosition()
  }, [isOpened])

  useLayoutEffect(() => {
    if (elementId === '') {
      return
    }

    const element = document.getElementById(elementId)

    if (element !== null) {
      window.scrollTo({
        top: element.getBoundingClientRect().top + window.scrollY - 100,
        behavior: 'smooth',
      })
      setElementId('')
    }
  }, [elementId])

  return (
    <header class={style.header}>
      <img class={style.logo} src={logoImage} alt='Логотип Kate Spark' />

      <div className={style.headerContacts}>
        <div className={style.tel}>
          <span>+7 (800) 555-30-53</span>
          <a onClick={contactMeClicked}>свяжитесь со мной</a>
        </div>
        <div className={style.address}>
          <address>Россия, г. Белгород</address>
          <address>ул. Королёва, д. 2а, к2, оф 512</address>
        </div>
      </div>

      <button onClick={toggleMenu} class={style.mobileNavButton}>
        <img className={style.menuIcon} src={menuIcon} alt='Меню' />
      </button>
      <nav className={style.mobileNav} data-opened={isOpened}>
        <div class={style.links}>
          <a onClick={onMobileLinkClick('about-us')}>О нас</a>
          <a onClick={onMobileLinkClick('employees')}>Специалисты</a>
          <a onClick={onMobileLinkClick('services')}>Услуги</a>
          <a onClick={onMobileLinkClick('prices')}>Тарифы</a>
          <a onClick={onMobileLinkClick('companies')}>Партнёры</a>
          <a onClick={onMobileLinkClick('contacts')}>Контакты</a>
        </div>
        <div className={style.tel}>
          <span>+7 (800) 555-30-53</span>
          <a onClick={contactMeClicked}>свяжитесь со мной</a>
        </div>
        <address>Россия, г. Белгород<br />ул. Королёва, д. 2а, к2, оф 512</address>
      </nav>
    </header>
  )
}

export default Header

import { h } from 'preact'
import style from './style.scss'

export const Button = (props) => {
  const onClick = () => {
    if (props.disabled) {
      return
    }

    if (typeof props.onClick === 'function') {
      props.onClick()
    }
  }

  return (
    <button
      type={props.type ?? 'submit'}
      class={style.button}
      onClick={onClick}
      data-color={props.color}
      data-disabled={props.disabled}
    >
      {props.children}
    </button>
  )
}

import { h } from 'preact'
import style from './style.scss'
import { useEffect, useLayoutEffect, useRef, useState } from 'preact/hooks'
import { memo } from 'preact/compat'

export const Input = memo((props) => {
  const ref = useRef(null)
  const nameRef = useRef(props.name ?? '')
  const submitInputRef = useRef(null)
  const [value, setValue] = useState(props.value ?? '')

  const onInput = (e) => {
    // костыль для placeholder в Safari
    if (e.target.innerText.trim() === '') {
      e.target.innerText = ''
    }

    setValue(e.target.innerText)
  }

  useEffect(() => {
    if (typeof props.onChange === 'function') {
      props.onChange({ name: nameRef.current, value })
    }
  }, [value, props.name, props.onChange])

  useLayoutEffect(() => {
    if (ref.current !== null) {
      ref.current.innerText = value

      const disableMultilineWithEnter = (e) => {
        if (e.key === 'Enter') {
          e.preventDefault()

          if (!submitInputRef.current) {
            return
          }

          submitInputRef.current.click()
        }
      }

      ref.current.addEventListener('keydown', disableMultilineWithEnter)

      return () => ref.current?.removeEventListener('keydown', disableMultilineWithEnter)
    }
  }, [])

  const hasError = props.errors?.length > 0

  return (
    <div class={style.wrapper} data-hasErrors={hasError} data-disabled={props.disabled}>
      <input ref={submitInputRef} type='submit' style={{ display: 'none' }} />
      <div
        ref={ref}
        class={style.input}
        contentEditable={!props.disabled}
        placeholder={props.placeholder}
        onInput={onInput}
      />
      {hasError && <div class={style.errorText}>{props.errors[0]}</div>}
    </div>
  )
})

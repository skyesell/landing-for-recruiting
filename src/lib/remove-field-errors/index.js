export const removeFieldErrors = (fieldName, errors) => {
  const withoutErrors = { ...errors }
  delete withoutErrors[fieldName]

  return withoutErrors
}

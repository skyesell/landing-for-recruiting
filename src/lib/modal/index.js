import { h, createContext } from 'preact'
import { useCallback, useContext, useState } from 'preact/hooks'

const ModalContext = createContext({
  contactManagerIsOpened: false,
  applyIsOpened: false,
  ratesIsOpened: false,
  selectedRate: null,
  setContactManagerModalState: () => {},
  setRatesModalState: () => {},
  setApplyModalState: () => {},
})

export const useModal = () => useContext(ModalContext)

export const ModalProvider = (props) => {
  const [modalState, setModalState] = useState({
    contactManagerIsOpened: false,
    applyIsOpened: false,
    ratesIsOpened: false,
    selectedRate: null,
  })

  const setContactManagerModalState = useCallback(
    (state) =>
      setModalState({
        contactManagerIsOpened: state,
        ratesIsOpened: false,
        applyIsOpened: false,
      }),
    []
  )

  const setRatesModalState = useCallback(
    (state, selectedRate) =>
      setModalState({
        contactManagerIsOpened: false,
        ratesIsOpened: state,
        applyIsOpened: false,
        selectedRate,
      }),
    []
  )

  const setApplyModalState = useCallback(
    (state) =>
      setModalState({
        contactManagerIsOpened: false,
        ratesIsOpened: false,
        applyIsOpened: state,
      }),
    []
  )

  return (
    <ModalContext.Provider
      value={{ ...modalState, setContactManagerModalState, setRatesModalState, setApplyModalState }}
    >
      {props.children}
    </ModalContext.Provider>
  )
}

import axios from 'axios'
import { delay } from '../lib/delay'

const http = axios.create({ baseURL: '/api' })

http.interceptors.response.use(undefined, (error) => {
  return Promise.reject(error.response?.data)
})

export const api = {
  apply: (applyForm) => delay(500).then(() => http.post('/apply', applyForm)),
  contactManager: (contactForm) =>
    delay(500).then(() => http.post('/contact-manager', contactForm)),
  rates: (ratesForm) => delay(500).then(() => http.post('/rates', ratesForm)),
}

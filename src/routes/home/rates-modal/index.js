import { h } from 'preact'
import { Modal } from '../../../components/modal'
import { Checkbox } from '../../../components/checkbox'
import { Button } from '../../../components/button'
import { Input } from '../../../components/input'
import { useModal } from '../../../lib/modal'
import style from './style.scss'
import { useCallback, useState } from 'preact/hooks'
import { api } from '../../../api'
import { removeFieldErrors } from '../../../lib/remove-field-errors'

export const RatesModal = () => {
  const { setRatesModalState, selectedRate } = useModal()

  const [fields, setFields] = useState({
    values: { name: '', phone: '', personalDataProcessing: false },
    errors: { name: [], phone: [], personalDataProcessing: [] },
  })

  const [pending, setPending] = useState(false)

  const { values, errors } = fields

  const hasErrors =
    typeof errors === 'object' && Object.values(errors).some((errors) => errors.length)

  const fieldChanged = useCallback(
    (e) =>
      setFields((state) => ({
        ...state,
        errors: removeFieldErrors(e.name, state.errors),
        values: { ...state.values, [e.name]: e.value },
      })),
    []
  )

  const closeRatesModal = () => setTimeout(setRatesModalState, 0, false)

  const errorsChanged = useCallback(
    (error) => setFields((state) => ({ ...state, errors: error.fieldErrors })),
    []
  )

  const formSubmitted = (e) => {
    e.preventDefault()

    if (hasErrors || pending) {
      return
    }

    setPending(true)

    api
      .rates({ ...values, selectedRate })
      .then(closeRatesModal)
      .catch(errorsChanged)
      .finally(() => setPending(false))
  }

  return (
    <Modal onClose={closeRatesModal}>
      <form className={style.content} onSubmit={formSubmitted}>
        <p className={style.title}>
          Наш менеджер свяжется с Вами и&nbsp;обсудит условия выбранного тарифа
        </p>

        <Input
          name='name'
          placeholder='Имя'
          value={values.name}
          errors={errors.name}
          disabled={pending}
          onChange={fieldChanged}
        />
        <Input
          name='phone'
          placeholder='Телефон'
          value={values.phone}
          errors={errors.phone}
          disabled={pending}
          onChange={fieldChanged}
        />

        <div className={style.personalData}>
          <Checkbox
            name='personalDataProcessing'
            value={values.personalDataProcessing}
            errors={errors.personalDataProcessing}
            disabled={pending}
            onChange={fieldChanged}
          />
          <span>Я соглашаюсь на обработку персональных данных</span>
        </div>

        <Button disabled={pending ||
            hasErrors}>Обсудить условия тарифа</Button>
      </form>
    </Modal>
  )
}

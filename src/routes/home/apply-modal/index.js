import { h } from 'preact'
import { useCallback, useState } from 'preact/hooks'
import { Modal } from '../../../components/modal'
import { Checkbox } from '../../../components/checkbox'
import { Button } from '../../../components/button'
import { Input } from '../../../components/input'
import { useModal } from '../../../lib/modal'
import { api } from '../../../api'
import style from './style.scss'
import { removeFieldErrors } from '../../../lib/remove-field-errors'

const fieldValuesInitialState = {
  name: '',
  phone: '',
  department: '',
  email: '',
  cv: '',
  position: '',
  personalDataProcessing: false,
}

const fieldErrorsInitialState = {
  name: [],
  phone: [],
  department: [],
  email: [],
  cv: [],
  position: [],
  personalDataProcessing: [],
}

export const ApplyModal = () => {
  const { setApplyModalState } = useModal()

  const [fields, setFields] = useState({
    values: fieldValuesInitialState,
    errors: fieldErrorsInitialState,
  })

  const [pending, setPending] = useState(false)

  const { values, errors } = fields

  const hasErrors =
    typeof errors === 'object' && Object.values(errors).some((errors) => errors.length)

  const closeApplyModal = () => setTimeout(setApplyModalState, 0, false)

  const fieldChanged = useCallback(
    (e) =>
      setFields((state) => ({
        ...state,
        errors: removeFieldErrors(e.name, state.errors),
        values: { ...state.values, [e.name]: e.value },
      })),
    []
  )

  const errorsChanged = useCallback(
    (error) => setFields((state) => ({ ...state, errors: error.fieldErrors })),
    []
  )

  const formSubmitted = (e) => {
    e.preventDefault()

    if (hasErrors || pending) {
      return
    }

    setPending(true)

    api
      .apply(fields.values)
      .then(closeApplyModal)
      .catch(errorsChanged)
      .finally(() => setPending(false))
  }

  return (
    <Modal onClose={closeApplyModal}>
      <form class={style.content} onSubmit={formSubmitted}>
        <p class={style.title}>Отправка резюме</p>
        <p class={style.label}>Станьте частью нашей большой команды</p>

        <Input
          name='name'
          placeholder='Имя'
          value={values.name}
          errors={errors.name}
          disabled={pending}
          onChange={fieldChanged}
        />
        <Input
          name='position'
          placeholder='Должность'
          value={values.position}
          errors={errors.position}
          disabled={pending}
          onChange={fieldChanged}
        />
        <Input
          name='cv'
          placeholder='Ссылка на резюме или портфолио'
          value={values.cv}
          errors={errors.cv}
          disabled={pending}
          onChange={fieldChanged}
        />
        <Input
          name='phone'
          placeholder='Телефон'
          value={values.phone}
          errors={errors.phone}
          disabled={pending}
          onChange={fieldChanged}
        />
        <Input
          name='email'
          placeholder='Email'
          value={values.email}
          errors={errors.email}
          disabled={pending}
          onChange={fieldChanged}
        />
        <Input
          name='department'
          placeholder='Где бы вы хотели работать?'
          value={values.department}
          errors={errors.department}
          disabled={pending}
          onChange={fieldChanged}
        />

        <div class={style.personalData}>
          <Checkbox
            name='personalDataProcessing'
            value={values.personalDataProcessing}
            errors={errors.personalDataProcessing}
            disabled={pending}
            onChange={fieldChanged}
          />
          <span>Я соглашаюсь на обработку персональных данных</span>
        </div>

        <Button
          disabled={
            pending ||
            hasErrors
          }
        >
          Отправить резюме
        </Button>
      </form>
    </Modal>
  )
}

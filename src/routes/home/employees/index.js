import { h } from 'preact'
import { useCallback, useRef, useState } from 'preact/hooks'
import openedIcon from './opened.svg'
import closedIcon from './closed.svg'
import cornerImage from './corner.svg'
import style from './style.scss'

const developers = [
  { title: 'Frontend', content: 'JavaScript (React, Angular, Vue.JS, jQuery), HTML, CSS и др.' },
  {
    title: 'Backend',
    content:
      'Java (Spring Framework, Hibernate, Spring Boot, Spring Security 5, Spring Cloud), Python (Django, Flask, web2py), Node.JS (Nest.JS, Express, Fastify, Sequelize, Prisma), PHP ( Laravel, Yii, CodeIgniter, Symfony, CakePHP, Zend Framework, Phalcon, FuelPHP), Ruby, SQL, Golang, С#, Kotlin, TypeScript, Rust, Scala и др.',
  },
  { title: 'iOS/Android', content: 'Java, Kotlin, Swift, Objective-C, React Native, Flutter' },
]
const analysts = [
    {title: 'Бизнес-аналитики', content: 'BPMN, EPC, E/R, UML, SQL, XML, WS, API (SOAP/REST), FlowChart, Figma, Miro, ARIS, BPWin PM, Fiddler, Draw.io Confluence, Jira и др.'},
    {title: 'Системные аналитики', content: 'BPMN, UML, SQL, API (SOAP/REST), SOAP UI, Postman, Swagger, XML, JSON, Draw.io, Confluence, Jira и др.'}
]

const testers = [
    {content: 'SQL, test case, API (SOAP/REST), Swagger, Postman, SOAP UI, XML, JSON, Fiddler, jMeter, TestLink и др.'},
]

const sysadmins = [
    {content: 'K8S, Rancher 2, Helm, Gitlab CI, TeamCity, Jenkins, DevOps, IaC, Agile. и др.'},
]
const designers = [
    {content: 'UI-kit, Figma, Adobe Illustrator, Adobe Photoshop, Adobe Lightroom, MS PowerPoint и др.'},
]
const pm = [
    {content: 'Agile, Scrum, Kanban, Confluence, MS Project, Jira и др.'},
]
export const Employees = () => (
  <div class={style.employees} id='employees'>
    <span>Кого ищете?</span>
    <span>Выберите специалистов себе в команду по направлениям</span>
    <Dropdown title='Бизнес-и-системные аналитики' list={analysts} />
    <EmployeePopup title='Бизнес-и-системные аналитики' list={analysts} />
    <Dropdown title='Разработчики' list={developers} />
    <EmployeePopup title='Разработчики' list={developers} />
    <Dropdown title='Тестировщики' list={testers} />
    <EmployeePopup title='Тестировщики' list={testers} />
    <Dropdown title='Системные администраторы' list={sysadmins} />
    <EmployeePopup title='Системные администраторы' list={sysadmins} />
    <Dropdown title='UX/UI дизайнеры' list={designers} />
    <EmployeePopup title='UX/UI дизайнеры' list={designers} />
    <Dropdown title='Product и Project менеджеры' list={pm} />
    <EmployeePopup title='Product и Project менеджеры' list={pm} />
  </div>
)

const Dropdown = ({ title, list }) => {
  const [isOpened, setIsOpened] = useState(false)

  const icon = isOpened ? openedIcon : closedIcon

  const onDropdownClick = () => {
    setIsOpened((state) => !state)
  }

  const doNotTriggerDropdownState = (e) => e.stopPropagation()

  return (
    <div class={style.dropdown} data-opened={isOpened} onClick={onDropdownClick}>
      <div class={style.heading}>
        <img src={icon} alt='' />
        <span class={style.title}>{title}</span>
      </div>
      <div class={style.list} onClick={doNotTriggerDropdownState}>
        {list.map((item, index) => (
          <div class={style.item} key={index}>
            <span class={style.itemTitle}>{item.title}</span>
            <span class={style.itemContent}>{item.content}</span>
          </div>
        ))}
      </div>
    </div>
  )
}

const EmployeePopup = ({ title, list }) => {
  const titleRef = useRef(null)
  const listRef = useRef(null)
  const mouseEntered = useRef(false)

  const changePopupPosition = useCallback(() => {
    if (titleRef.current === null || listRef.current === null || !mouseEntered.current) {
      return
    }

    titleRef.current.style = 'color: #0816B7'

    const rect = titleRef.current.getBoundingClientRect()

    listRef.current.style = `display: block; position: absolute; left: ${rect.right}px; top: -60px; z-index: 100;`
  }, [])

  const hidePopup = useCallback(() => {
    if (listRef.current === null) {
      return
    }

    titleRef.current.style = ''
    listRef.current.style = ''
  }, [])

  const onMouseEnter = () => {
    mouseEntered.current = true
    changePopupPosition()
  }

  const onMouseLeave = () => {
    mouseEntered.current = false
    hidePopup()
  }

  return (
    <div class={style.employeePopup}>
      <div class={style.heading}>
        <span class={style.title} ref={titleRef} onMouseEnter={onMouseEnter} onMouseLeave={onMouseLeave}>
          {title}
        </span>
        <div className={style.list} ref={listRef} onMouseEnter={onMouseEnter} onMouseLeave={onMouseLeave}>
          <div class={style.invisible} />
          <img className={style.corner} src={cornerImage} alt='' />
          {list.map((item, index) => (
            <div className={style.item} key={index}>
              <span className={style.itemTitle}>{item.title}</span>
              <span className={style.itemContent}>{item.content}</span>
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}

import { h } from 'preact'
import style from './style.scss'
import tnImage from '../../../assets/tn.svg'

export const TN = () => (
  <div class={style.tn}>
    <p>Разработка сайта —</p>
    <img src={tnImage} alt='НПО ТН' loading='lazy' />
  </div>
)

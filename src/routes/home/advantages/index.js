import { h } from 'preact'
import bgMobile from './bg-mobile.svg'
import bgTablet from './bg-tablet.svg'
import bgDesktopS from './bg-desktop-s.svg'
import bgDesktopLg from './bg-desktop-lg.svg'
import style from './style.scss'

export const Advantages = () => (
  <div class={style.advantages} id='about-us'>
    <picture class={style.bg}>
      <source media='(min-width: 1920px)' srcSet={bgDesktopLg} />
      <source media='(min-width: 1200px)' srcSet={bgDesktopS} />
      <source media='(min-width: 768px)' srcSet={bgTablet} />
      <source media='(min-width: 320px)' srcSet={bgMobile} />
      <img src={bgMobile} alt='' />
    </picture>

    <div class={style.wrapper}>
      <span>Кто мы такие</span>
      <span>Мы экономим Ваше время, силы и средства на поиск необходимого специалиста</span>

      <div class={style.text}>
        <div class={style.content}>
          <span>Опытные руки, намётанный глаз</span>
          <span>
            Более 10 лет работаем на рынке IT, знаем&nbsp;подводные&nbsp;камни, качественный подход к&nbsp;каждому
            специалисту
          </span>
        </div>
        <div class={style.content}>
          <span>Долгосрочное сотрудничество</span>
          <span>
            К нам возвращаются 90% наших партнёров, обучаем&nbsp;будущих соискателей работе у Вас
          </span>
        </div>
        <div class={style.content}>
          <span>Собственная база резюме</span>
          <span>
            Обучаем специалистов на базовых кафедрах и&nbsp;в&nbsp;собственном корпоративном университете
          </span>
        </div>
      </div>
    </div>
  </div>
)

import { h } from 'preact'
import manImage from '../../../assets/man.png'
import manDesktopImage from '../../../assets/man2.png'
import bgMobile from './bg-mobile.svg'
import bgTablet from './bg-tablet.svg'
import bgDesktopS from './bg-desktop-s.svg'
import bgDesktopLg from './bg-desktop-lg.svg'
import style from './style.scss'

export const About = () => (
  <div class={style.about}>
    <picture class={style.bg}>
      <source media='(min-width: 1920px)' srcSet={bgDesktopLg} />
      <source media='(min-width: 1200px)' srcSet={bgDesktopS} />
      <source media='(min-width: 768px)' srcSet={bgTablet} />
      <source media='(min-width: 320px)' srcSet={bgMobile} />
      <img src={bgMobile} alt='' />
    </picture>

    <div class={style.content}>
      <Recruitment />
      <Description />
      <Man />
      <Companies />
      <ManDesktop />
    </div>
  </div>
)

const Recruitment = () => (
  <div>
    <h1 className={style.heading}>
      Подбор
      <br />
      IT—Персонала
    </h1>
  </div>
)

const Description = () => (
  <p class={style.description}>
    <span>Рекрутинговое агенство </span>
    <span>
      «Кейт Спарк» специализируется в подборе ИТ-специалистов для российских компаний в сферах
      финансов, страхования, ритейла и логистики.
    </span>
  </p>
)

const Man = () => (
  <div className={style.man}>
    <img src={manImage} alt='Человек ищет персонал в Kate Spark' loading='lazy' />
  </div>
)

const ManDesktop = () => (
  <div className={style.manDesktop}>
    <img src={manDesktopImage} alt='Человек ищет персонал в Kate Spark' loading='lazy' />
  </div>
)

const Companies = () => (
  <p class={style.companies}>
    Мы уже нашли IT-специалистов в команды Хоум Кредит Банк, Открытие, БСС, IBS, ВТБ, VK и др.
  </p>
)

import { h } from 'preact'
import style from './style.scss'
import logoImage from '../../../assets/logo2.svg'
import { useModal } from '../../../lib/modal'

export const Footer = () => {
  const modalController = useModal()

  const contactMeClicked = (e) => {
    e.preventDefault()
    modalController.setContactManagerModalState(true)
  }

  const onLinkClick = (elementId) => () => {
    const element = document.getElementById(elementId)

    if (element !== null) {
      window.scrollTo({
        top: element.getBoundingClientRect().top + window.scrollY - 15,
        behavior: 'smooth',
      })
    }
  }

  return (
    <div class={style.footer}>
      <div class={style.logo}>
        <img src={logoImage} alt='Логотип Kate Spark' />
        <div className={style.info}>
          <p>© 2010 — 2022 Корпорация «КейтСпарк».<br />Все права защищены.</p>
          <a href='https://google.com' target='_blank' rel='noreferrer'>
            Политика конфиденциальности
          </a>
        </div>
      </div>

      <div class={style.nav}>
        <a onClick={onLinkClick('about-us')}>О нас</a>
        <a onClick={onLinkClick('employees')}>Специалисты</a>
        <a onClick={onLinkClick('services')}>Услуги</a>
        <a onClick={onLinkClick('prices')}>Тарифы</a>
        <a onClick={onLinkClick('companies')}>Партнёры</a>
        <a onClick={onLinkClick('contacts')}>Контакты</a>
      </div>

      <div class={style.addressInfo}>
        <div class={style.address}>
          <address>Россия, г. Белгород</address>
          <address>ул. Королёва, д. 2а, к2, оф 512</address>
        </div>
        <div class={style.contacts}>
          <span>+7 (800) 555-30-53</span>
          <a onClick={contactMeClicked}>свяжитесь со мной</a>
        </div>
      </div>
    </div>
  )
}

import { h } from 'preact'
import { About } from './about'
import { Advantages } from './advantages'
import { Employees } from './employees'
import { Apply } from './apply'
import { Prices } from './prices'
import { Companies } from './companies'
import { Contacts } from './contacts'
import { TN } from './tn'
import Header from '../../components/header'
import Navigation from '../../components/navigation'
import Layout from '../../components/layout'
import { Background } from '../../components/background'
import { Footer } from './footer'
import { Maps } from './maps'
import { ContactManagerModal } from './contact-manager-modal'
import { ApplyModal } from './apply-modal'
import { RatesModal } from './rates-modal'
import { useModal } from '../../lib/modal'

const Home = () => {
  const { contactManagerIsOpened, ratesIsOpened, applyIsOpened } = useModal()

  return (
    <>
      {contactManagerIsOpened && <ContactManagerModal />}

      {ratesIsOpened && <RatesModal />}

      {applyIsOpened && <ApplyModal />}

      <Background color='blue'>
        <Layout>
          <Header />
        </Layout>
      </Background>

      <Background color='blue'>
        <Layout>
          <Navigation />
        </Layout>
      </Background>

      <Background color='blue'>
        <Layout>
          <About />
        </Layout>
      </Background>

      <Layout>
        <Advantages />
      </Layout>

      <Layout>
        <Employees />
      </Layout>

      <Layout>
        <Apply />
      </Layout>

      <Background color='blue'>
        <Layout>
          <Prices />
        </Layout>
      </Background>


      <Layout>
        <Companies />
      </Layout>

      <Layout>
        <Contacts />
      </Layout>

      <Maps />

      <Background color='black'>
        <Layout>
          <Footer />
        </Layout>
      </Background>

      <Background color='blue'>
        <Layout>
          <TN />
        </Layout>
      </Background>
    </>
  )
}

export default Home

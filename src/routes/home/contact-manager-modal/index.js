import { h } from 'preact'
import { useCallback, useState } from 'preact/hooks'
import { Modal } from '../../../components/modal'
import { Checkbox } from '../../../components/checkbox'
import { Button } from '../../../components/button'
import { Input } from '../../../components/input'
import { useModal } from '../../../lib/modal'
import style from './style.scss'
import { api } from '../../../api'
import { removeFieldErrors } from '../../../lib/remove-field-errors'

export const ContactManagerModal = () => {
  const { setContactManagerModalState, setApplyModalState } = useModal()

  const [fields, setFields] = useState({
    values: { name: '', phone: '', personalDataProcessing: false },
    errors: { name: [], phone: [], personalDataProcessing: [] },
  })

  const [pending, setPending] = useState(false)

  const { values, errors } = fields

  const hasErrors =
    typeof errors === 'object' && Object.values(errors).some((errors) => errors.length)

  const fieldChanged = useCallback(
    (e) =>
      setFields((state) => ({
        ...state,
        errors: removeFieldErrors(e.name, state.errors),
        values: { ...state.values, [e.name]: e.value },
      })),
    []
  )

  const errorsChanged = useCallback(
    (error) => setFields((state) => ({ ...state, errors: error.fieldErrors })),
    []
  )

  const closeContactManagerModal = () => setTimeout(setContactManagerModalState, 0, false)

  const formSubmitted = (e) => {
    e.preventDefault()

    if (hasErrors || pending) {
      return
    }

    setPending(true)

    api
      .contactManager(fields.values)
      .then(closeContactManagerModal)
      .catch(errorsChanged)
      .finally(() => setPending(false))
  }

  const applyClicked = () => {
    setContactManagerModalState(false)
    setApplyModalState(true)
  }

  return (
    <Modal onClose={closeContactManagerModal}>
      <form className={style.content} onSubmit={formSubmitted}>
        <p className={style.title}>Наш менеджер<br />готов связаться с Вами</p>

        <Input
          name='name'
          placeholder='Имя'
          value={values.name}
          errors={errors.name}
          disabled={pending}
          onChange={fieldChanged}
        />
        <Input
          name='phone'
          placeholder='Телефон'
          value={values.phone}
          errors={errors.phone}
          disabled={pending}
          onChange={fieldChanged}
        />

        <div className={style.personalData}>
          <Checkbox
            name='personalDataProcessing'
            value={values.personalDataProcessing}
            errors={errors.personalDataProcessing}
            disabled={pending}
            onChange={fieldChanged}
          />
          <span>Я соглашаюсь на обработку персональных данных</span>
        </div>

        <div>
          <Button disabled={pending ||
              hasErrors}>Заказать звонок</Button>
          <Button color='white' onClick={applyClicked}>
            Отправить резюме
          </Button>
        </div>
      </form>
    </Modal>
  )
}

import { h } from 'preact'
import { useState } from 'preact/hooks'
import style from './style.scss'

export const Maps = () => {
  const [isLoaded, setIsLoaded] = useState(false)

  const onLoad = () => {
    setIsLoaded(true)

    DG.then(() => {
      const map = DG.map('mapRoot', {
        center: { lon: 36.571247, lat: 50.571265 },
        fullscreenControl: false,
        zoom: 20,
      })

      DG.marker({ lon: 36.571247, lat: 50.571265 }, { title: 'Мы здесь' }).addTo(map)
    })
  }

  return (
    <div class={style.maps}>
      <div id='mapRoot' class={style.mapsRoot}>
        {!isLoaded && <div class={style.fallback}>Загрузка...</div>}
      </div>
      <script crossorigin='anonymous' src='https://maps.api.2gis.ru/2.0/loader.js' async onLoad={onLoad} />
    </div>
  )
}

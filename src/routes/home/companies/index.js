import { h } from 'preact'
import hcbImage from './hcb.png'
import hcbColoredImage from './hcb-colored.png'
import openBankImage from './open-bank.png'
import openBankColoredImage from './open-bank-colored.png'
import ibsImage from './ibs.png'
import ibsColoredImage from './ibs-colored.png'
import vkImage from './vk.png'
import vkColoredImage from './vk-colored.png'
import bssImage from './bss.png'
import bssColoredImage from './bss-colored.png'
import vtbImage from './vtb-bank.png'
import vtbColoredImage from './vtb-bank-colored.png'
import alphaImage from './alpha.png'
import alphaColoredImage from './alpha-colored.png'
import bksImage from './bks.png'
import bksColoredImage from './bks-colored.png'
import style from './style.scss'
import { useState } from 'preact/hooks'

export const Companies = () => (
  <div class={style.companies} id='companies'>
    <span>Кто пользуется услугами?</span>
    <span>Эти компании больше не беспокоятся за квалификацию персонала</span>
    <div class={style.cards}>
      <Card src={hcbImage} coloredSrc={hcbColoredImage} alt='Home Credit Bank' />
      <Card src={openBankImage} coloredSrc={openBankColoredImage} alt='Открытие банк' />
      <Card src={ibsImage} coloredSrc={ibsColoredImage} alt='IBS' />
      <Card src={vkImage} coloredSrc={vkColoredImage} alt='VK' />
      <Card src={bssImage} coloredSrc={bssColoredImage} alt='BSS' />
      <Card src={vtbImage} coloredSrc={vtbColoredImage} alt='Банк ВТБ' />
      <Card src={alphaImage} coloredSrc={alphaColoredImage} alt='Альфа Капитал' />
      <Card src={bksImage} coloredSrc={bksColoredImage} alt='БКС' />
    </div>
  </div>
)

const Card = (props) => {
  const [hover, setHover] = useState(false)

  const changeHoverState = (value) => () => setHover(value)

  return (
    <div
      class={style.card}
      onMouseEnter={changeHoverState(true)}
      onMouseLeave={changeHoverState(false)}
    >
      <img
        src={props.src}
        alt={props.alt}
        loading='lazy'
        style={{ display: hover ? 'none' : 'block' }}
      />
      <img
        src={props.coloredSrc}
        alt={props.alt}
        loading='lazy'
        style={{ display: hover ? 'block' : 'none' }}
      />
    </div>
  )
}

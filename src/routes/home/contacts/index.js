import { h } from 'preact'
import geoImage from '../../../assets/geo.svg'
import phoneImage from '../../../assets/phone.svg'
import mailImage from '../../../assets/mail.svg'
import googleMapsImage from '../../../assets/google-maps.png'
import twoGisImage from '../../../assets/2gis.png'
import style from './style.scss'
import { useModal } from '../../../lib/modal'
import { ApplyMini } from '../apply-mini'

export const Contacts = () => {
  const modalController = useModal()

  const contactMeClicked = (e) => {
    e.preventDefault()
    modalController.setContactManagerModalState(true)
  }

  return (
    <div class={style.contacts} id='contacts'>
      <span>Мы ждём вас</span>
      <span>Остались вопросы? Ответим любым удобным способом :)</span>

      <div class={style.links}>
        <div class={style.contactApply}>
          <Contact title='Адрес' src={geoImage}>
            <address class={style.address}>
              308034, Россия, г. Белгород, ул. Королёва, д. 2а, корпус 2, офис 512
            </address>
          </Contact>
          <div style={{ fontStyle: 'normal', width: '250px'}}>
            <Contact title='Приёмная' src={phoneImage}>
              <span style={{width: '180px'}}>+7 (800) 555-30-53</span>
              <a onClick={contactMeClicked}>свяжитесь со мной</a>
            </Contact>
          </div>
        </div>
        <div class={style.contactApply}>
            <div style={{marginRight: '100px'}}>
          <Contact title='Менеджер' src={mailImage} style={{ marginTop: '3px' }}>
            <a
              href='mailto: kate.spark@reliab.tech'
              style={{
                textDecoration: 'none',
                color: 'unset',
                fontSize: '18px',
                lineHeight: '21px',
              }}
            >
              kate.spark@reliab.tech
            </a>
          </Contact>
            </div>
          <ApplyMini />
        </div>
      </div>

      <div class={style.externalLinks}>
        <ExternalLink
          href='https://goo.gl/maps/dA2FnxH2HRdPNMie9'
          src={googleMapsImage}
          alt='Google Карты'
        />
        <ExternalLink
          href='https://2gis.ru/belgorod/geo/6474560119525973'
          src={twoGisImage}
          alt='2GIS'
        />
      </div>
    </div>
  )
}

const Contact = (props) => (
  <div class={style.contact}>
    <img src={props.src} alt='' loading='lazy' style={props.style} />
    <div class={style.content}>
      <span>{props.title}</span>
      {props.children}
    </div>
  </div>
)

const ExternalLink = (props) => (
  <a class={style.externalLink} href={props.href} target='_blank' rel='noreferrer'>
    <img src={props.src} alt={props.alt} loading='lazy' />
  </a>
)

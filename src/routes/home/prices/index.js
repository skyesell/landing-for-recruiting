import { h } from 'preact'
import { useModal } from '../../../lib/modal'
import cn from 'classnames'
import rateIcon from './rate.svg'
import style from './style.scss'
import bgDesktop from './bgDesktop.svg'

export const Prices = () => {
  const modalController = useModal()

  const rateClicked = (selectedRate) => () => modalController.setRatesModalState(true, selectedRate)

  return (
      <div>
    <div class={style.prices}>
      <span id='services'>Что из всего этого наша работа</span>
      <span>Мы тщательно подходим к работе</span>

      <div class={style.cards}>
        <Card class={style.interviewCard}>
          <span>
            Детально интервьюируем и&nbsp;отсеиваем&nbsp;специалистов согласно&nbsp;предъявленным требованиям
          </span>
        </Card>
        <Card class={style.consultingCard}>
          Выступаем как рекрутеры и&nbsp;консультанты, информируем заказчика о ситуации на рынке
        </Card>
        <Card class={style.supportCard}>
          Поддерживаем связь с кандидатом на&nbsp;всех этапах трудоустройства
        </Card>
        <Card class={style.offerCard}>Работаем с возражениями и&nbsp;контр-офферами</Card>
      </div>
      <span id='prices'>Что по цене?</span>
      <span>Выберите свой вариант сотрудничества</span>

      <div class={style.rates}>
        <Rate
          offerButtonClassName={style.basedButton}
          title='Тариф «Базовый»'
          buttonText='Выбрать тариф «Базовый»'
          about={[
            'Персональный менеджер и индивидуальный подход',
            'Единоразовая выплата комиссии после выхода кандидата',
            'Гарантированная однократная замена',
            'Динамическое ценообразование',
            'Гибкая система скидок',
          ]}
          onClick={rateClicked('Тариф «Базовый»')}
        />

        <Rate
          offerButtonClassName={style.advancedButton}
          title='Тариф «Продвинутый»'
          buttonText='Выбрать тариф «Продвинутый»'
          about={[
            'Персональный менеджер и индивидуальный подход',
            'Выплата комиссии равными долями в течении согласованного периода (после выхода кандидата)',
            'Количество замен не ограничено',
            'Первое резюме в течении 8 рабочих часов',
            'По запросу предоставляется аналитика рынка по вакансии',
            'Возможность скрининга кандидатов и технического интервью силами наших экспертов по стандартам заказчика',
            'Гибкая система скидок',
          ]}
          borderColor='green'
          onClick={rateClicked('Тариф «Продвинутый»')}
        />
        <Rate
          offerButtonClassName={style.eliteButton}
          title='Тариф «Под ключ»'
          buttonText='Выбрать тариф «Под ключ»'
          about={[
            'Обучим специалистов в согласованный срок под требования заказчика и специфику его проектов',
            'Оплата только после приема кандидатов в штат заказчика',
            'Итоговая оценка кандидата со стороны заказчика и по его собственным критериям',
            'Заказчик в праве отказаться от кандидата без пенальти',
            'Прозрачный процесс обучения с эффективной обратной связью',
            'Гарантия замены специалиста',
            'Скидки при длительном сотрудничестве',
            'Индивидуальный подход',
          ]}
          borderColor='pink'
          onClick={rateClicked('Тариф «Под ключ»')}
        />
          <img className={style.bg} media='(min-width: 1200px)' src={bgDesktop} />
      </div>

    </div>

      </div>
  )
}

const Card = (props) => <div class={cn(style.card, props.class)}>{props.children}</div>

const Rate = (props) => (
  <div class={style.rate} data-border-color={props.borderColor}>
    <span>{props.title}</span>
    {props.about.map((item, index) => (
      <div class={style.listItem} key={index}>
        <img class={style.rateIcon} src={rateIcon} alt='' loading='lazy' />
        <span>{item}</span>
      </div>
    ))}
    <button class={cn(style.rateButton, props.offerButtonClassName)} onClick={props.onClick}>
      <img class={style.rateIcon} src={rateIcon} alt='' loading='lazy' />
      <span>{props.buttonText}</span>
    </button>
  </div>
)

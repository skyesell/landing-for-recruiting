module.exports = function (config) {
  if (process.env.NODE_ENV !== 'production') {
    config.devServer.proxy = [
      {
        path: '/api/**',
        target: 'http://localhost:5000',
        changeOrigin: true,
        changeHost: true,
      },
    ]
  }
}
